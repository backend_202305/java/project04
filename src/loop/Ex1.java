package loop;

/*
 * 숫자 1부터 10까지 출력하세요
 * */

public class Ex1 {

	public static void main(String[] args) {
		System.out.println(1);
		System.out.println(2);
		System.out.println(3);
		System.out.println(4);
		System.out.println(5);
		System.out.println(6);
		System.out.println(7);
		System.out.println(8);
		System.out.println(9);
		System.out.println(10);
		//우리가 배운 코드만으로 작성한다면 print문을 10번 사용해야 한다
		//이렇게 반복되는일을 처리하기 위해 사용되는 것이 "반복문" 이다
	}

}
