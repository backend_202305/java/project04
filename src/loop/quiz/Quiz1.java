package loop.quiz;

/*
 * while문을 사용해 1부터 10까지 합을 구하고 출력하세요
 * */

public class Quiz1 {

	public static void main(String[] args) {

		int num = 1; //더할 숫자
		int sum = 0; //합계를 저장할 변수
		
		while(num <= 10){ //조건을 만족하는 동안 총 10번 코드를 실행한다
			sum = sum + num; // num을 1씩 증가시켜서 sum에 계속 더한다
//			System.out.println("num: " + num + ",sum: " + sum); //중간과정
			num++; //1,2,3,4,5,6,7,8,9,10
		}
		System.out.println("1부터10까지의 합은 " + sum + "입니다.");
	}
}

// 프로그램에 필요한 변수를 선언한다
// -> 합계와 더할수
// 조건식을 작성한다
// -> num이 1부터 10이 될때까지
// 프로그램의 흐름을 작성한다
// -> num을 1씩 증가시키면서 sum에 더한다



