package loop;

/*
 * for문을 사용하여 숫자 1부터 10까지 출력하기
 * */
public class Ex4 {

	public static void main(String[] args) {

		 //초기화, 조건, 증감
		for(int i=1; i<=10; i++){ //i가 1부터 10까지 1씩 증가되므로, 블록을 10번 반복 수행한다
			// i = 1,2,3,4,5,6,7,8,9,10
			System.out.println(i); 
		}
		
	}
}

